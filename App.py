from flask import Flask, jsonify

app = Flask('jenkins-test')


class Response:
    status_code = ''
    message = ''
    response = ''

    def __init__(self, status_code, message):
        self.status_code = status_code
        self.message = message


@app.route('/')
def hello():
    result = Response('200', 'Hello! This is a test for a jenkins integration')
    return jsonify(status=result.status_code, message=result.message)


@app.route('/<name>')
def hello_name(name):
    result = Response('200', 'Hello %s! This is a test for a jenkins integration' % name)
    return jsonify(status=result.status_code, message=result.message)


if __name__ == '__main__':
    app.run(debug=True, host="*", port="8181")
